# tauri-react-app



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/fedelleos/tauri-react-app.git
git branch -M main
git push -uf origin main
```



## Instalacion
<pre>
npm install 
cd tauri-react-app
nano src/store/snippetsStore.tsx
Cambiamos, export var path = 'Nuestro directorio raiz'
npm run tauri dev
</pre>

## Name
Tauri-React-App

## Description
Se trata de realizar una aplicacion de escritorio, en este caso un editor, que usa la sintaxis de vscode.

La ventaja que nos da Tauri es realizar aplicacion de escritorio con elementos web como TypeScript y en este caso React

La carpeta origen la fijamos en src/store/snippetStore.tsx

## Badges
<img src="public/badge.png" />

## Visuals
<img src="/public/1.png">
<img src="/public/2.png">
<img src="/public/3.png">
<img src="/public/4.png">
<img src="/public/5.png">
