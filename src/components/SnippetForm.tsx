import { writeTextFile } from '@tauri-apps/api/fs'
import { useSnippetStore } from '../store/snippetsStore'
import { useState } from 'react'
import { toast} from 'react-hot-toast'
import { path } from '../store/snippetsStore'

export default function SnippetForm() {
  const [snippetName, setSnippetName] = useState('')
  const addSnippetName = useSnippetStore(state => state.addSnippetName)
  const snippetNames = useSnippetStore(state => state.snippetsNames)
 
  return (
    
    <form onSubmit={async(e) => {     
      e.preventDefault()
      const fileTosave = `${path}/${snippetName}`
      console.log(fileTosave+', file to save')
      await writeTextFile(fileTosave, `{}`)
      setSnippetName('')
      console.log(snippetName+', snipet name')
      if(snippetNames.filter(n => n == snippetName).length == 0) {
        addSnippetName(snippetName)
        toast.success('Snippet Saved', {
          duration: 2000,
          position: "bottom-right",
          style: {
            background: "#202020",
            color: "#fff"
          }
        })
      } else {
        toast.error('Snippet name already exist', {
          duration: 2000,
          position: "bottom-right",
          style: {
            background: "#202020",
            color: "#fff"
          }
        })
      }
     
    }}>
     

      {/* New file */}
      <input type="text"
        placeholder="Write a file"
        className="bg-zinc-900 w-full border-none outline-none p-4"
        onChange={(e)=> setSnippetName(e.target.value)}        
        value={snippetName}
        maxLength={30}
      />
      <button className="hidden">
        Save
      </button>
    </form>
  )
}

