import { Editor } from '@monaco-editor/react'
import { useSnippetStore, path } from '../store/snippetsStore'
import { useEffect, useState } from 'react'
import { writeTextFile } from '@tauri-apps/api/fs'
import { TfiPencil } from 'react-icons/tfi'
import { carpeta } from './SnippetItem'

export var selectedSnippet: any

export function clearSelectedSnippet() {
  selectedSnippet.name = ''
}
export default function SnippetEditor() {
  selectedSnippet = useSnippetStore(state => state.selectedSnippet)
  const [text, setText] = useState<string | undefined>('')
  //Grabado automatico
  useEffect(() => {
    console.log(selectedSnippet)
    if (selectedSnippet == null) return;
    if (carpeta == false) {
      console.log(carpeta)
      const file = `${path}/${selectedSnippet.name}`
      const saveText = setTimeout(() => {
        writeTextFile(file, text ?? '')
        console.log(file + ', saved')
      }, 1000)
      return () => {
        clearTimeout(saveText)
      }
    }
  }, [text])
  const ext = selectedSnippet?.name.split('.')[1]
  var language = ''
  switch (ext) {
    case 'js': { language = 'javascript'; break; }
    case 'rb': { language = 'ruby'; break; }
    case 'tsx': { language = 'typescript'; break; }
    default: { language = ext!; break; }
  }
  return (
   
    <>
      {selectedSnippet && carpeta==false ? (
        <Editor
          theme="vs-dark"
          language={language}
          options={{
            fontSize: 15
          }}
          onChange={value => setText(value)}
          value={selectedSnippet.code == '{}' ? '' : selectedSnippet.code}
        />
      ) : (
        <TfiPencil className='text-9xl text-neutral-900' />
      )}
    </>)
    
}

