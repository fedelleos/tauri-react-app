import { useEffect } from "react"
import { readDir } from "@tauri-apps/api/fs"
import { setPath, path, useSnippetStore } from "../store/snippetsStore"
import SnippetItem, { reload, setCarpeta } from "./SnippetItem"

export default function SnippetList() {
  const setSnippetsNames = useSnippetStore((state) => state.setSnippetsNames)
  const snippetNames = useSnippetStore(state => state.snippetsNames)
  const setSelectedSnippet = useSnippetStore(state => state.setSelectedSnippet)
  var result:any
  useEffect(() => {
    async function loadFiles() {
      console.log(path)
      result = await readDir(path)
      console.log(result)
      const filenames = result.map((file:any) => file.name!.split('*')[0])
      setSnippetsNames(filenames)
    }
    loadFiles()
  }, [])

  return (
    <div>
      {/* Atras */}
      <button
        onClick={async (e) => {
          setSelectedSnippet({ name: path, code: '' }) //para marcarlo
          setCarpeta(true)
          e.preventDefault()
          let r = path.split('/')
          while (r[r.length - 1] == '') r.pop()
          console.log(r)
          var es = ''
          for (let i = 0; i < r.length - 1; i++) {
            if (i < r.length - 2) { es = es + r[i] + '/' }
            else {
              es = es + r[i]; setPath(es); console.log(es);
              reload()
            }
          }
          
        }}>{path}</button>
      {snippetNames.map(snippetName => (
        <SnippetItem key={snippetName} snippetName={snippetName} />
      ))}
    </div>
  )
}

