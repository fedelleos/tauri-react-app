import { setPath, path, useSnippetStore } from "../store/snippetsStore";
import { twMerge } from "tailwind-merge"; //Estilos condicionados 
import { readTextFile, removeFile, readDir } from '@tauri-apps/api/fs'
import toast from 'react-hot-toast'
import { FiTrash } from 'react-icons/fi'
import { createRoot } from 'react-dom/client';
import React from "react";
import App from "../App";

export var carpeta:boolean = false
export function setCarpeta(set:boolean) { carpeta = set}

interface Props {
  snippetName: string;
}

export default function SnippetItem({ snippetName }: Props) {
  const setSelectedSnippet = useSnippetStore(state => state.setSelectedSnippet)
  const selectedSnippet = useSnippetStore(state => state.selectedSnippet)
  const removeSnippetName = useSnippetStore(stat => stat.removeSnippetName)
  //Borrar archivo
  const handleDelete = async (snippetName: string) => {
    const file = `${path}/${snippetName}`
    try {
      await readDir(file)
      toast.custom('Es un directorio', {
        duration: 2000,
        position: "bottom-right",
        style: {
          background: "red",
          color: "white",
        }
      })
    } catch {
      const accept = await window.confirm('Are you sure you want to delete this file?')
      if (!accept) return
      else {
        removeFile(file)
        removeSnippetName(snippetName)
        toast.success('Snippet Deleted', {
          duration: 2000,
          position: "bottom-right",
          style: {
            background: "#202020",
            color: "#fff"
          }
        })
      }
    }
  }
  //Pinta
  return (
    <div
      className={
        twMerge(
          "flex justify-between py-2 px-4 hover:bg-neutral-600 hover:cursor-pointer",
          selectedSnippet?.name === snippetName ? "bg-blue-500" : "bg-zinc-900"
        )
      }
      onClick={async () => {
        const textfile = `${path}/${snippetName}`
        try {
          const snippet = await readTextFile(textfile)
          carpeta = false
          setSelectedSnippet({ name: snippetName, code: snippet })
          console.log(textfile + ', leido fichero')
          
        } catch (e) {
          try {
            setPath(textfile)
            setSelectedSnippet({ name: snippetName, code: '' }) //para marcarlo
            carpeta = true
            console.log(path + ', leido directorio')
            reload()
          } catch (e) {
            console.log(e)
          }
        }
      }}
    >
      <h1>{snippetName}</h1>
      <div className="flex gap-2 items-end">
        <FiTrash onClick={(e) => {
          e.stopPropagation()
          handleDelete(snippetName)
        }}
          className="text-red-500"
        />
        { }
      </div>
    </div>
  )

}

export function reload() {
  //reacarga
    const div = document.getElementById("div")
    var root = createRoot(div!);
    root.render(
      <React.StrictMode>
        <App />
      </React.StrictMode>
    )
}
