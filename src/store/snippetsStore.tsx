import { create } from "zustand";
import { selectedSnippet } from "../components/SnippetEditor";

interface Snippet {
    name: string;
    code: string ;
}

interface SnippetState {
    snippetsNames: string[]
    selectedSnippet: Snippet | null
    addSnippetName: (name: string) => void;
    setSnippetsNames: (names: string[]) => void;
    setSelectedSnippet: (snippet: Snippet | null) => void;
    removeSnippetName: (name: string) => void;
}
export var path = 'B:/'
export const useSnippetStore = create<SnippetState>((set) => ({
    snippetsNames: [],
    selectedSnippet: null,
    addSnippetName: (name) => 
        set((state) => ({
            snippetsNames: [...state.snippetsNames, name],
    })),
    setSnippetsNames: (names) => set({ snippetsNames: names}),
    setSelectedSnippet: (snippet) => set({ selectedSnippet: snippet}),
    removeSnippetName: name => set (state => ({
        snippetsNames: state.snippetsNames.filter(n => n !== name)
    }))
}))

export function setPath(newpath:string) {
    path = newpath
}